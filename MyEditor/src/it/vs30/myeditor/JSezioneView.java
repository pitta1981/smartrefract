/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * JSezioneView.java
 *
 * Created on 9-gen-2012, 22.32.27
 */
package it.vs30.myeditor;

//import org.vs30.modpenetrometria.*;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.TexturePaint;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;
import java.awt.image.BufferedImage;
import org.myorg.myapi.APIObject;
import org.myorg.myapi.DrawingAPI;
import org.myorg.myapi.FirstBrakeList;
import org.myorg.myapi.Indagine;

/**
 *
 * @author pitta1981
 */
public class JSezioneView extends javax.swing.JPanel {

    private Indagine proj;
    private double margUp = 0.07;
    Jsezdlg sezDlg = new Jsezdlg(this);
    public BufferedImage bIm2, bIm1;
    public BufferedImage bIm3;
    boolean isWhite,proporz;
    
    //get, set method
    public boolean isProporz(){
        
        return proporz;
    }
    public void setProporz(boolean b){
        proporz=b;
        obj.proporz=b;
    }
    
    public boolean isBgWhite(){
        
        return isWhite;
    }
    public void setBgWhite(boolean b){
        isWhite=b;
    }
    
    
    private double maxv;
    public APIObject obj;
    public DrawingAPI dAPI;

    /** Creates new form JSezioneView */
    public JSezioneView() {
        initComponents();
    }

    JSezioneView(APIObject par_obj) {
        // throw new UnsupportedOperationException("Not yet implemented");
        initComponents();
        obj = par_obj;
        this.proj = par_obj.proj;
        dAPI=new DrawingAPI(proj, obj);

    }
    
    public void setObj(APIObject par_obj){
        obj = par_obj;
        this.proj = par_obj.proj;
        dAPI=new DrawingAPI(proj, obj);

    }

    @Override
    public void paintComponent(Graphics g) {


        super.paintComponent(g);
       // proj.checkLic();
        proporz=obj.proporz;
        isWhite = obj.is_white;
        if (isWhite) {
            this.setBackground(Color.white);

        } else {
            this.setBackground(Color.BLACK);
        }
        
      //  dAPI=new DrawingAPI(proj, obj);
        //sezione.draw();
       // dAPI.drawSezAssi(g,this.getWidth(), this.getHeight());
        double[] mxmn=dAPI.maxmin();
        FirstBrakeList ArrFB = proj.stesa.get(0);
      //  double[] mxmn=dAPI.maxmin();
      //  FirstBrakeList ArrFB = (FirstBrakeList) proj.stesa.get(0);
        double ascismax=(ArrFB.ch-1)*ArrFB.spaz;
        double x_step=(this.getWidth()/ArrFB.fb.length);
        double x=(this.getWidth()-(2*dAPI.margine_dx)-(2*dAPI.margine_sx))/ascismax;
        double y=x;
        double profmx=mxmn[0]+mxmn[1];
        double rap=(profmx*y)+120;
        double scala=1;
        //margup=0.07
        //ystp = (ymax - (2.3 * margUp * ymax) - 30) / mxmn;
        
        if(rap>this.getHeight()){
           // scala=(this.getHeight()-120)/rap;
            x=(this.getHeight()-120)/profmx;
            rap=(this.getHeight());
            y=x;
       
        }
        if(!obj.proporz){
            
            x=(this.getWidth()-(2*dAPI.margine_dx)-(2*dAPI.margine_sx))/ascismax;
            y=(this.getHeight()-120)/profmx;
            rap=(profmx*y)+120;
            
        }
        
        
        
     //   drawAssi(g, this.getWidth(), this.getHeight());
        dAPI.drawSezAssi(g,this.getWidth(), (int)(rap),x,y);
        
        dAPI.drawSezione2(g, (this.getWidth()), (int)(rap),x,y);
        
        //drawSezione(g, this.getWidth(), this.getHeight());
       
  //      PenetrometroGraph pg = new PenetrometroGraph(new int[]{4, 3, 6, 3, 6, 9, 12, 14, 11, 10, 16, 19, 40, 60}, 25, 0.5);
        //pg.draw(g, margUp,xshf, this.getWidth(), this.getHeight(), this.maxv,((FirstBrakeList) proj.stesa.get(0)).spaz_in,((FirstBrakeList) proj.stesa.get(0)).spaz*(((FirstBrakeList) proj.stesa.get(0)).ch-1));


        if (!proj.licenza) {
            drawNoLicenza(g, this.getWidth(), this.getHeight());
        }
        //disegnaSegnale(g);
        //disegnaPrimoA(g);

    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                formMouseClicked(evt);
            }
        });
        addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                formMouseMoved(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

private void formMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseClicked
/*
    sezDlg.V2 = proj.V2A;
    sezDlg.tV1 = proj.Tv1;
    sezDlg.tV2 = proj.Tv2;
    sezDlg.spaz = proj.VfB_A[1].spaz;
    sezDlg.setProj(proj);

    //sezDlg.setProj(proj);
    sezDlg.setLocation(evt.getXOnScreen() + 10, evt.getYOnScreen() - 20);
    if (obj.is_white) {
        sezDlg.color_setText("Black background");
    } else {
        sezDlg.color_setText("White background");
    }
    
    if (obj.proporz) {
            sezDlg.prop_setText("Maximized profile");
        } else {
            sezDlg.prop_setText("Proportional profile");
        }
    
    
    try {
        //AWTUtilities.setWindowOpacity(sezDlg, 0.5f);
    } catch (Exception ex) {
    }
    sezDlg.setVisible(true);*/
    // TODO add your handling code here:
}//GEN-LAST:event_formMouseClicked

    private void formMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseMoved
        // TODO add your handling code here:
        try {
           // AWTUtilities.setWindowOpacity(sezDlg, 0.5f);
        } catch (Exception ex) {
        }

    }//GEN-LAST:event_formMouseMoved
    public int xshf = 0;

    public void drawAssi(Graphics g, int w, int h) {
        FirstBrakeList ArrFB = (FirstBrakeList) proj.stesa.get(0);
        int ymax = h;//this.getHeight();
        int xmax = w;//this.getWidth();
        int nchanel = xmax / ArrFB.fb.length;
        int ychn = (ymax - (0 * nchanel)) / nchanel;
        xshf = nchanel;
        if (!isWhite) {
            g.setColor(Color.white);
        } else {
            g.setColor(Color.black);
        }

        g.drawLine((int) (xshf * 0.75), (int) ((margUp * ymax)), (int) (xshf * 0.75), (int) (ymax - (margUp * ymax)));
        g.drawLine(xmax - (int) (xshf * 0.75), (int) ((margUp * ymax)), xmax - (int) (xshf * 0.75), (int) (ymax - (margUp * ymax)));
        g.drawLine((int) (xshf * 0.75), (int) (ymax - (margUp * ymax)), xmax - (int) (xshf * 0.75), (int) (ymax - (margUp * ymax)));
        g.drawLine((int) (xshf * 0.75), (int) ((margUp * ymax)), xmax - (int) (xshf * 0.75), (int) ((margUp * ymax)));
    }

    public void drawSezione(Graphics g, int w, int h) {
        double ystp = 0.0;
        FirstBrakeList ArrFB = (FirstBrakeList) proj.stesa.get(0);
        int ymax = h;//this.getHeight();
        int xmax = w;//this.getWidth();
        int nchanel = xmax / ArrFB.fb.length;
        //int nchanel =(int)(step*ArrFB.spaz);
     //   int ychn = (int) (ymax - (0 * nchanel)) / nchanel;
        xshf = nchanel;
        Graphics2D g2 = (Graphics2D) g.create();
        Path2D p = new Path2D.Double();
        Path2D p2 = new Path2D.Double();

        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);


        g.drawLine((int) (xshf * 0.75), (int) ((margUp * ymax) + 30), xmax - (int) (xshf * 0.75), (int) ((margUp * ymax) + 30));
        g.setColor(Color.YELLOW);
        /*for (int i = 0; i < ArrFB.fb.length - 1; i++) {
        
        drawGeo(g, (int) (xshf + (nchanel * i)), (int) ((margUp * ymax) + 15));
        }*/
        double max = -999;
        FirstBrakeList fbl = (FirstBrakeList) proj.stesa.get(0);


        if (proj.Zg2.length > 1) {
            //     g.drawString("ZG 2", 20, 60);


            for (int i = 0; i < proj.Zg1.length - 1; i++) {
                if (proj.Zg1[i] - fbl.fb[i].z > max) {
                    max = proj.Zg1[i] - fbl.fb[i].z;
                }
            }
            if (proj.Zg2.length > 1) {
                for (int i = 0; i < proj.Zg2.length - 1; i++) {

                    if (proj.Zg2[i] - fbl.fb[i].z > max) {
                        max = proj.Zg2[i] - fbl.fb[i].z;
                    }
                }
            }
            double min = -9999;
            for (int i = 0; i < proj.Zg1.length - 1; i++) {
                if (fbl.fb[i].z > min) {
                    min = fbl.fb[i].z;
                }
            }


            this.maxv = max;
            drawtick(g, max, min, w, h);

            //ystp = (ymax - (2.3 * margUp * ymax) - 30) / max;

            double mxmn = min + max;

            ystp = (ymax - (2.3 * margUp * ymax) - 30) / mxmn;
            //ystp=step;

            for (int i = 0; i < ArrFB.fb.length - 1; i++) {

                drawGeo(g, (int) (xshf + (nchanel * i)), (int) ((margUp * ymax) + 15 - ((ArrFB.fb[i].z - min) * ystp)));
            }



            BufferedImage bi = new BufferedImage(5, 5,
                    BufferedImage.TYPE_INT_RGB);
            Graphics2D big = bi.createGraphics();
            big.setColor(Color.CYAN);
            big.fillRect(0, 0, 5, 5);
            big.setColor(Color.DARK_GRAY);
            big.fillOval(0, 0, 4, 4);
            Rectangle r = new Rectangle(0, 0, 5, 5);

            if (bIm2 == null) {
                r = new Rectangle(0, 0, 5, 5);
                g2.setPaint(new TexturePaint(bi, r));
            } else {
                r = new Rectangle(0, 0, bIm2.getWidth(), bIm2.getHeight());
                g2.setPaint(new TexturePaint(bIm2, r));

            }
            //g2.setPaint(new TexturePaint(bi, r));




            p2 = new Path2D.Double();
            p2.moveTo((xshf + (nchanel * 0)), ystp * (proj.Zg2[0] - fbl.fb[0].z + min) + (margUp * ymax) + 30);
            for (int i = 0; i < proj.Zg1.length - 3; i++) {
                p2.curveTo((xshf + (nchanel * i)), ystp * (proj.Zg2[i] - fbl.fb[i].z + min) + (margUp * ymax) + 30, (xshf + (nchanel * (i - 0.3))), ystp * (proj.Zg2[i] - fbl.fb[i].z + min) + (margUp * ymax) + 30, (xshf + (nchanel * (i + 0.3))), ystp * (proj.Zg2[i] - fbl.fb[i].z + min) + (margUp * ymax) + 30);
            }
            p2.curveTo((xshf + (nchanel * (proj.Zg2.length - 3))), ystp * (proj.Zg2[(proj.Zg2.length - 3)] - fbl.fb[(proj.Zg1.length - 3)].z + min) + (margUp * ymax) + 30, (xshf + (nchanel * ((proj.Zg2.length - 3) - 0.3))), ystp * (proj.Zg2[(proj.Zg2.length - 3)] - fbl.fb[(proj.Zg1.length - 3)].z + min) + (margUp * ymax) + 30, (xshf + (nchanel * (proj.Zg2.length - 3))), ystp * (proj.Zg2[(proj.Zg2.length - 3)] - fbl.fb[(proj.Zg1.length - 3)].z + min) + (margUp * ymax) + 30);

            p2.lineTo((xshf + (nchanel * (proj.Zg2.length - 3))), (-(fbl.fb[(proj.Zg1.length - 3)].z - min) * ystp) + (margUp * ymax) + 30);

            for (int i = proj.Zg2.length - 3; i > 0; i--) {
                p2.lineTo((xshf + (nchanel * i)), (-(fbl.fb[i].z - min) * ystp) + (margUp * ymax) + 30);

            }

            p2.lineTo((xshf + (nchanel * 0)), (-(fbl.fb[0].z - min) * ystp) + (margUp * ymax) + 30);
            p2.lineTo((xshf + (nchanel * 0)), ystp * (proj.Zg2[0] - fbl.fb[0].z + min) + (margUp * ymax) + 30);
            g2.draw(p2);
            g2.fill(p2);

        }




        if (proj.Zg1.length > 1) {
            //  g.drawString("ZG 1", 20, 30);

            for (int i = 0; i < proj.Zg1.length - 1; i++) {
                if (proj.Zg1[i] - fbl.fb[i].z > max) {
                    max = proj.Zg1[i] - fbl.fb[i].z;
                }
            }
            if (proj.Zg2.length > 1) {
                for (int i = 0; i < proj.Zg2.length - 1; i++) {

                    if (proj.Zg2[i] - fbl.fb[i].z > max) {
                        max = proj.Zg2[i] - fbl.fb[i].z;
                    }
                }
            }


            double min = -9999;
            for (int i = 0; i < proj.Zg1.length - 1; i++) {
                if (fbl.fb[i].z > min) {
                    min = fbl.fb[i].z;
                }
            }

            drawtick(g, max, min, w, h);
            //ystp = (ymax - (2.3 * margUp * ymax) - 30) / max;

            double mxmn = min + max;

            ystp = (ymax - (2.3 * margUp * ymax) - 30) / mxmn;

            for (int i = 0; i < ArrFB.fb.length - 1; i++) {

                drawGeo(g, (int) (xshf + (nchanel * i)), (int) ((margUp * ymax) + 15 - ((ArrFB.fb[i].z - min) * ystp)));
            }


            BufferedImage bi = new BufferedImage(5, 5,
                    BufferedImage.TYPE_INT_RGB);
            Graphics2D big = bi.createGraphics();
            big.setColor(Color.GREEN);
            big.fillRect(0, 0, 5, 5);
            big.setColor(Color.lightGray);
            big.fillOval(0, 0, 5, 5);
            if (bIm1 == null) {
                Rectangle r = new Rectangle(0, 0, 5, 5);
                g2.setPaint(new TexturePaint(bi, r));
            } else {
                Rectangle r = new Rectangle(0, 0, bIm1.getWidth(), bIm1.getHeight());
                g2.setPaint(new TexturePaint(bIm1, r));

            }



            p = new Path2D.Double();
            p.moveTo((xshf + (nchanel * 0)), ystp * (proj.Zg1[0] - fbl.fb[0].z + min) + (margUp * ymax) + 30);
            for (int i = 0; i < proj.Zg1.length - 3; i++) {
                p.curveTo((xshf + (nchanel * i)), ystp * (proj.Zg1[i] - fbl.fb[i].z + min) + (margUp * ymax) + 30, (xshf + (nchanel * (i - 0.3))), ystp * (proj.Zg1[i] - fbl.fb[i].z + min) + (margUp * ymax) + 30, (xshf + (nchanel * (i + 0.3))), ystp * (proj.Zg1[i] - fbl.fb[i].z + min) + (margUp * ymax) + 30);
            }
            p.curveTo((xshf + (nchanel * (proj.Zg1.length - 3))), ystp * (proj.Zg1[(proj.Zg1.length - 3)] - fbl.fb[proj.Zg1.length - 3].z + min) + (margUp * ymax) + 30, (xshf + (nchanel * ((proj.Zg1.length - 3) - 0.3))), ystp * (proj.Zg1[(proj.Zg1.length - 3)] - fbl.fb[(proj.Zg1.length - 3)].z + min) + (margUp * ymax) + 30, (xshf + (nchanel * (proj.Zg1.length - 3))), ystp * (proj.Zg1[(proj.Zg1.length - 3)] - fbl.fb[(proj.Zg1.length - 3)].z + min) + (margUp * ymax) + 30);


            p.lineTo((xshf + (nchanel * (proj.Zg1.length - 3))), (-(fbl.fb[(proj.Zg1.length - 3)].z - min) * ystp) + (margUp * ymax) + 30);

            for (int i = proj.Zg1.length - 3; i >= 0; i--) {
                p.lineTo((xshf + (nchanel * i)), (-(fbl.fb[i].z - min) * ystp) + (margUp * ymax) + 30);

            }


            p.lineTo((xshf + (nchanel * 0)), (-(fbl.fb[(proj.Zg1.length - 3)].z - min) * ystp) + (margUp * ymax) + 30);
            p.lineTo((xshf + (nchanel * 0)), ystp * (proj.Zg1[0] - fbl.fb[0].z + min) + (margUp * ymax) + 30);

            g2.draw(p);

            g2.fill(p);
            drawLeg(g2, proj.V1, 1, w, h);


            bi = new BufferedImage(5, 5,
                    BufferedImage.TYPE_INT_RGB);
            big = bi.createGraphics();
            big.setColor(Color.CYAN);
            big.fillRect(0, 0, 5, 5);
            big.setColor(Color.DARK_GRAY);
            big.fillOval(1, 1, 3, 3);
            Rectangle r = new Rectangle(0, 0, 5, 5);
            if (bIm2 == null) {
                r = new Rectangle(0, 0, 5, 5);
                g2.setPaint(new TexturePaint(bi, r));
            } else {
                r = new Rectangle(0, 0, bIm2.getWidth(), bIm2.getHeight());
                g2.setPaint(new TexturePaint(bIm2, r));

            }

            //g2.setPaint(new TexturePaint(bi, r));


            drawLeg(g2, proj.V2, 2, w, h);



            if (proj.Zg2.length > 1) {
                p2 = new Path2D.Double();
                p2.moveTo((xshf + (nchanel * 0)), ystp * (proj.Zg2[0] - fbl.fb[0].z + min) + (margUp * ymax) + 30);
                for (int i = 0; i < proj.Zg1.length - 3; i++) {
                    p2.curveTo((xshf + (nchanel * i)), ystp * (proj.Zg2[i] - fbl.fb[i].z + min) + (margUp * ymax) + 30, (xshf + (nchanel * (i - 0.3))), ystp * (proj.Zg2[i] - fbl.fb[i].z + min) + (margUp * ymax) + 30, (xshf + (nchanel * (i + 0.3))), ystp * (proj.Zg2[i] - fbl.fb[i].z + min) + (margUp * ymax) + 30);
                }

                p2.curveTo((xshf + (nchanel * (proj.Zg2.length - 3))), ystp * ((proj.Zg2[(proj.Zg1.length - 3)] - fbl.fb[(proj.Zg1.length - 3)].z + min)) + (margUp * ymax) + 30, (xshf + (nchanel * ((proj.Zg2.length - 3) - 0.3))), ystp * (proj.Zg2[(proj.Zg2.length - 3)] - fbl.fb[(proj.Zg1.length - 3)].z + min) + (margUp * ymax) + 30, (xshf + (nchanel * (proj.Zg2.length - 3))), ystp * (proj.Zg2[(proj.Zg2.length - 3)] - fbl.fb[(proj.Zg1.length - 3)].z + min) + (margUp * ymax) + 30);

                p2.lineTo((xshf + (nchanel * (proj.Zg2.length - 3))), (ymax - (margUp * ymax * 1.1)));



                p2.lineTo((xshf + (nchanel * 0)), (ymax - (margUp * ymax * 1.1)));
                p2.lineTo((xshf + (nchanel * 0)), ystp * proj.Zg2[0] + (margUp * ymax) + 30);

                bi = new BufferedImage(5, 5,
                        BufferedImage.TYPE_INT_RGB);
                big = bi.createGraphics();
                big.setColor(Color.ORANGE);
                big.fillRect(0, 0, 5, 5);
                big.setColor(Color.yellow);
                big.fillOval(1, 1, 3, 3);
                r = new Rectangle(0, 0, 5, 5);

                if (bIm3 == null) {
                    r = new Rectangle(0, 0, 5, 5);
                    g2.setPaint(new TexturePaint(bi, r));
                } else {
                    r = new Rectangle(0, 0, bIm3.getWidth(), bIm3.getHeight());
                    g2.setPaint(new TexturePaint(bIm3, r));

                }

                //g2.setPaint(new TexturePaint(bi, r));
                g2.draw(p2);

                g2.fill(p2);
                drawLeg(g2, proj.V3, 3, w, h);


            } else {

                p2 = new Path2D.Double();
                p2.moveTo((xshf + (nchanel * 0)), ystp * (proj.Zg1[0] - fbl.fb[0].z + min) + (margUp * ymax) + 30);
                for (int i = 0; i < proj.Zg1.length - 3; i++) {
                    p2.curveTo((xshf + (nchanel * i)), ystp * (proj.Zg1[i] - fbl.fb[i].z + min) + (margUp * ymax) + 30, (xshf + (nchanel * (i - 0.3))), ystp * (proj.Zg1[i] - fbl.fb[i].z + min) + (margUp * ymax) + 30, (xshf + (nchanel * (i + 0.3))), ystp * (proj.Zg1[i] - fbl.fb[i].z + min) + (margUp * ymax) + 30);
                }

                p2.curveTo((xshf + (nchanel * (proj.Zg1.length - 3))), ystp * (proj.Zg1[(proj.Zg1.length - 3)] - fbl.fb[(proj.Zg1.length - 3)].z + min) + (margUp * ymax) + 30, (xshf + (nchanel * ((proj.Zg1.length - 3) - 0.3))), ystp * (proj.Zg1[(proj.Zg1.length - 3)] - fbl.fb[(proj.Zg1.length - 3)].z + min) + (margUp * ymax) + 30, (xshf + (nchanel * (proj.Zg1.length - 3))), ystp * (proj.Zg1[(proj.Zg1.length - 3)] - fbl.fb[(proj.Zg1.length - 3)].z + min) + (margUp * ymax) + 30);

                p2.lineTo((xshf + (nchanel * (proj.Zg1.length - 3))), (ymax - (margUp * ymax * 1.1)));
                p2.lineTo((xshf + (nchanel * 0)), (ymax - (margUp * ymax * 1.1)));
                p2.lineTo((xshf + (nchanel * 0)), ystp * (proj.Zg1[0] - fbl.fb[0].z + min) + (margUp * ymax) + 30);

                bi = new BufferedImage(5, 5,
                        BufferedImage.TYPE_INT_RGB);
                big = bi.createGraphics();
                big.setColor(Color.CYAN);
                big.fillRect(0, 0, 5, 5);
                big.setColor(Color.DARK_GRAY);
                big.fillOval(1, 1, 3, 3);
                r = new Rectangle(0, 0, 5, 5);

                if (bIm2 == null) {
                    r = new Rectangle(0, 0, 5, 5);
                    g2.setPaint(new TexturePaint(bi, r));
                } else {
                    r = new Rectangle(0, 0, bIm2.getWidth(), bIm2.getHeight());
                    g2.setPaint(new TexturePaint(bIm2, r));

                }


                //g2.setPaint(new TexturePaint(bi, r));
                g2.draw(p2);

                g2.fill(p2);

                drawLeg(g2, proj.V2, 2, w, h);

            }



        }





    }

    public void drawGeo(Graphics g, int x, int y) {
        if (!isWhite) {
            g.setColor(Color.YELLOW);
        } else {
            g.setColor(Color.BLUE);
        }
        g.drawLine(x - 5, y, x + 5, y);
        g.drawLine(x - 5, y, x, y + 10);
        g.drawLine(x + 5, y, x, y + 10);
        g.drawLine(x, y + 10, x, y + 20);
    }

    public void drawtick(Graphics g, double max, double min, int w, int h) {

        double ystp = 0.0;
        FirstBrakeList ArrFB = (FirstBrakeList) proj.stesa.get(0);
        int ymax = h;//this.getHeight();
        int xmax = w;//this.getWidth();
        int nchanel = xmax / ArrFB.fb.length;
        int ychn = (int) (ymax - (0 * nchanel)) / nchanel;
        int xshf = nchanel;
        int majTic = 0, minTic = 0;
        Graphics2D g2 = (Graphics2D) g.create();

        double mxmn = min + max;

        ystp = (ymax - (2.3 * margUp * ymax) - 30) / mxmn;

        if (mxmn / 10.0 < 1) {
            majTic = 2;
            minTic = 1;

        } else if (mxmn / 10.0 < 2) {
            majTic = 3;
            minTic = 1;
        } else if (mxmn / 10.0 < 3) {
            majTic = 5;
            minTic = 2;
        }

        if (!isWhite) {
            g2.setColor(Color.white);
        } else {
            g2.setColor(Color.black);
        }

        //  g2.setColor(Color.white);

        g2.setFont(new Font("Dialog", Font.PLAIN, (int) (12 * (w / 1000.0))));
        //new Font("Dialog", Font.PLAIN, 12);

        FontMetrics fontMetrics = g2.getFontMetrics();


        int height = fontMetrics.getHeight();


        for (int j = (int) min; j > -max; j = j - minTic) {
            int i = -(j - (int) min);
            if (i % majTic == 0) {
                g2.drawLine((int) (xshf * 0.75), (int) (i * ystp + (margUp * ymax) + 30), (int) (xshf * 0.6), (int) (i * ystp + (margUp * ymax) + 30));
                int width = fontMetrics.stringWidth("" + (i));

                g2.drawString("" + j, (int) (xshf * 0.55) - width, (int) (i * ystp + (margUp * ymax) + 30 + (height / 3)));
                g2.drawLine((int) (xmax - xshf * 0.75), (int) (i * ystp + (margUp * ymax) + 30), (int) (xmax - xshf * 0.6), (int) (i * ystp + (margUp * ymax) + 30));

            } else {
                g2.drawLine((int) (xshf * 0.75), (int) (i * ystp + (margUp * ymax) + 30), (int) (xshf * 0.65), (int) (i * ystp + (margUp * ymax) + 30));
                g2.drawLine((int) (xmax - xshf * 0.75), (int) (i * ystp + (margUp * ymax) + 30), (int) (xmax - xshf * 0.65), (int) (i * ystp + (margUp * ymax) + 30));
            }
        }


        for (int i = 0; i < ArrFB.fb.length - 1; i++) {

            g2.drawLine( (xshf + (nchanel * i)), (int) ((margUp * ymax)), (int) (xshf + (nchanel * i)), (int) ((margUp * ymax) * 0.85));
            int width = fontMetrics.stringWidth("" + String.format("%.2g%n", (i * ArrFB.spaz + ArrFB.spaz_in)));

            g2.drawString("" + String.format("%.2g%n", (i * ArrFB.spaz + ArrFB.spaz_in)), (int) (xshf + (nchanel * i) - (width / 2)), (int) ((margUp * ymax) * 0.83));
        }






    }

    public void drawtick(Graphics g, double max, int w, int h) {
        double ystp = 0.0;
        FirstBrakeList ArrFB = (FirstBrakeList) proj.stesa.get(0);
        int ymax = h;//this.getHeight();
        int xmax = w;//this.getWidth();
        int nchanel = xmax / ArrFB.fb.length;
        int ychn = (int) (ymax - (0 * nchanel)) / nchanel;
        int xshf = nchanel;
        int majTic = 0, minTic = 0;
        Graphics2D g2 = (Graphics2D) g.create();

        ystp = (ymax - (2.3 * margUp * ymax) - 30) / max;

        if (max / 10.0 < 1) {
            majTic = 2;
            minTic = 1;

        } else if (max / 10.0 < 2) {
            majTic = 3;
            minTic = 1;
        } else if (max / 10.0 < 3) {
            majTic = 5;
            minTic = 2;
        }

        if (!isWhite) {
            g2.setColor(Color.white);
        } else {
            g2.setColor(Color.black);
        }

        //  g2.setColor(Color.white);

        g2.setFont(new Font("Dialog", Font.PLAIN, (int) (12 * (w / 1000.0))));
        new Font("Dialog", Font.PLAIN, 12);

        FontMetrics fontMetrics = g2.getFontMetrics();


        int height = fontMetrics.getHeight();


        for (int i = 0; i < max; i = i + minTic) {
            if (i % majTic == 0) {
                g2.drawLine((int) (xshf * 0.75), (int) (i * ystp + (margUp * ymax) + 30), (int) (xshf * 0.6), (int) (i * ystp + (margUp * ymax) + 30));
                int width = fontMetrics.stringWidth("" + (i));

                g2.drawString("" + i, (int) (xshf * 0.55) - width, (int) (i * ystp + (margUp * ymax) + 30 + (height / 3)));
                g2.drawLine((int) (xmax - xshf * 0.75), (int) (i * ystp + (margUp * ymax) + 30), (int) (xmax - xshf * 0.6), (int) (i * ystp + (margUp * ymax) + 30));

            } else {
                g2.drawLine((int) (xshf * 0.75), (int) (i * ystp + (margUp * ymax) + 30), (int) (xshf * 0.65), (int) (i * ystp + (margUp * ymax) + 30));
                g2.drawLine((int) (xmax - xshf * 0.75), (int) (i * ystp + (margUp * ymax) + 30), (int) (xmax - xshf * 0.65), (int) (i * ystp + (margUp * ymax) + 30));
            }
        }


        for (int i = 0; i < ArrFB.fb.length - 1; i++) {

            g2.drawLine((int) (xshf + (nchanel * i)), (int) ((margUp * ymax)), (int) (xshf + (nchanel * i)), (int) ((margUp * ymax) * 0.85));
            int width = fontMetrics.stringWidth("" + (i * ArrFB.spaz + ArrFB.spaz_in));

            g2.drawString("" + (i * ArrFB.spaz + ArrFB.spaz_in), (int) (xshf + (nchanel * i) - (width / 2)), (int) ((margUp * ymax) * 0.83));
        }





    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables

    private void drawLeg(Graphics2D g2, double V1, int i, int w, int h) {
        double ystp = 0.0;
        FirstBrakeList ArrFB = (FirstBrakeList) proj.stesa.get(0);
        int ymax = h;//this.getHeight();
        int xmax = w;//this.getWidth();
        int nchanel = xmax / ArrFB.fb.length;
        int ychn = (int) (ymax - (0 * nchanel)) / nchanel;
        int xshf = nchanel;
        int majTic = 0, minTic = 0;
        Path2D p = new Path2D.Double();
        double posx = xshf + ((i - 1) * (xmax / 3));
        p.moveTo(posx, ymax - (ymax * margUp) + 5);
        p.lineTo(posx + 20, ymax - (ymax * margUp) + 5);
        p.lineTo(posx + 20, ymax - 5);
        p.lineTo(posx, ymax - 5);
        g2.draw(p);

        g2.fill(p);


        FontMetrics fontMetrics = g2.getFontMetrics();
        g2.setFont(new Font("Dialog", Font.PLAIN, (int) (12 * (w / 1000.0))));

        int height = fontMetrics.getHeight();
        // g2.setColor(Color.white);
        if (!isWhite) {
            g2.setColor(Color.white);
        } else {
            g2.setColor(Color.black);
        }

        g2.drawString("" + (int) (V1 * 1000) + " m/s", (float) posx + 30, ymax - 5 - (height / 3));


    }

    private void drawNoLicenza(Graphics g, int x, int y) {
        Graphics2D g2 = (Graphics2D) g.create();
        Font font = new Font("Serif", Font.PLAIN, 36);
        g2.setFont(font);
        AffineTransform At = new AffineTransform();
        At.setToRotation(Math.toRadians(-45), 0.0f + (x * 0.1f),  (y - (y * 0.2f)));
        //At.rotate(Math.toRadians(-45));
        g2.setTransform(At);
        g2.setColor(Color.DARK_GRAY);

        g2.drawString("Trial - Non per uso commerciale", 0.0f + (x * 0.1f),  (y - (y * 0.1f)));
        /*At = new AffineTransform();
        At.setToRotation(Math.toRadians(-45), 0.0f+(x*0.3f), (float)(y-(y*0.15f)));
        //At.rotate(Math.toRadians(-45));
        g2.setTransform(At);*/
        g2.setColor(Color.DARK_GRAY);

        g2.drawString("Trial - Non per uso commerciale", 0.0f + (x * 0.2f),  (y + (y * 0.15f)));
        /* At = new AffineTransform();
        At.setToRotation(Math.toRadians(-45), 0.0f+(x*0.3f), (float)(y-(y*0.15f)));
        //At.rotate(Math.toRadians(-45));
        g2.setTransform(At);*/
        g2.setColor(Color.DARK_GRAY);

        g2.drawString("Trial - Non per uso commerciale", 0.0f + (x * 0.4f),  (y + (y * 0.65f)));


        g2.setColor(Color.DARK_GRAY);

        g2.drawString("Trial - Non per uso commerciale", 0.0f + (x * 0.45f), (y + (y * 0.95f)));

        g2.setColor(Color.GRAY);

        g2.drawString("Trial - Non per uso commerciale", 0.0f + (x * 0.3f),  (y + (y * 0.35f)));

        g2.setColor(Color.GRAY);

        g2.drawString("Trial - Non per uso commerciale", 0.0f + (x * 0.0f),  (y - (y * 0.30f)));


    }
}
