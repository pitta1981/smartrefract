/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

 /*
 * draw_geometria.java
 *
 * Created on 4-apr-2013, 22.23.15
 */
package it.vs30.myeditor;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author user
 */
public class draw_geometria extends javax.swing.JPanel {

    private boolean noGeom;

    /**
     * Creates new form draw_geometria
     */
    public draw_geometria() {
        initComponents();
    }

    @Override
    public void paintComponent(Graphics g) {

        super.paintComponent(g);
        boolean is_white = false;
        if (!is_white) {
            this.setBackground(Color.black);
        } else {
            this.setBackground(Color.white);
        }
        if (!noGeom) {
            try {

                g.setColor(Color.red);
                //  g.drawString(" "+shot+" "+spaz_in+" "+spaz+" "+nchan, 10, 20);

                double min = 999;
                double max = 0;

                for (int i = 0; i < shot.length; i++) {
                    if (shot[i] < spaz_in) {
                        if (shot[i] < min) {
                            min = shot[i];
                        }
                    } else {
                        if (spaz_in < min) {
                            min = spaz_in;
                        }
                    }

                    if (shot[i] > spaz_in
                            + ((nchan
                            - 1) * spaz) && shot[i] > max) {
                        max = shot[i];
                    } else {
                        max = spaz_in + ((nchan - 1) * spaz);
                    }
                }
                double dim = max - min;
                double step = (this.getWidth() - (this.getWidth() * 0.1)) / dim;

                double shift = (this.getWidth() * 0.1) / 2;

                //   g.drawLine((int)((shot-min)*step+shift), 20, (int)((shot-min)*step+shift), 40);
                for (int i = 0; i < shot.length; i++) {
                    drawShot(g, (int) ((shot[i] - min) * step + shift), 20);
                }
                g.setColor(Color.GRAY);
                g.drawLine(0, 30, this.getWidth(), 30);

                if (!is_white) {
                    g.setColor(Color.CYAN);

                } else {
                    g.setColor(Color.darkGray);
                }

                for (int i = 0; i < nchan; i++) {
                    //g.drawLine((int)((spaz_in+(spaz*i)-min)*step+shift), 20, (int)((spaz_in+(spaz*i)-min)*step+shift), 40);
                    drawGeo(g, (int) ((spaz_in + (spaz * i) - min) * step + shift), 20);
                }

            } catch (Exception ex) {
            }
        }
        else{
            g.setColor(Color.red);
            
            g.drawString("Geometry not defined", this.getWidth()/2-50, this.getHeight()/2-6);
            
            
        }
    }
    int nchan = 0;
    double shot[], spaz = 0, spaz_in = 0;

    public void drawGeo(Graphics g, int x, int y) {
        g.drawLine(x - 5, y, x + 5, y);
        g.drawLine(x - 5, y, x, y + 10);
        g.drawLine(x + 5, y, x, y + 10);
        g.drawLine(x, y + 10, x, y + 20);
    }

    public void drawShot(Graphics g, int x, int y) {
        g.drawRect(x - 5, y + 5, 10, 5);
        //g.drawLine(x-5,y+5,x+5,y+5);
        g.drawLine(x - 5, y + 5, x + 5, y + 10);
        g.drawLine(x + 5, y + 5, x - 5, y + 10);
        g.drawLine(x, y + 10, x, y + 20);
    }

    public void setGeom(double[] shot, double spaz, double spaz_in, int nchan) {
        this.shot = shot;
        this.spaz = spaz;
        this.spaz_in = spaz_in;
        this.nchan = nchan;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 316, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 242, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    void setNoGeom(boolean b) {
        noGeom = b;

//throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
