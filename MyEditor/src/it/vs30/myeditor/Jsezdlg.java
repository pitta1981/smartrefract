
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * Jsezdlg.java
 *
 * Created on 23-gen-2012, 23.23.39
 */
package it.vs30.myeditor;

//~--- non-JDK imports --------------------------------------------------------
import org.myorg.myapi.DrawingAPI;
import org.myorg.myapi.FirstBrakeList;
import org.myorg.myapi.Indagine;
//import org.myorg.myviewer.MyViewerTopComponent;

import org.openide.util.Lookup;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

//~--- JDK imports ------------------------------------------------------------

//import com.sun.awt.AWTUtilities;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import javax.imageio.ImageIO;

import javax.swing.JFileChooser;

/**
 *
 * @author user
 */
public class Jsezdlg extends javax.swing.JDialog {

    public double[] tV1;
    double spaz;
    JSezioneView sV;
    double[] tV2;
    private jTavolozzaDlg tavo_dlg;
    public double[] V2;
    public double[] V3;
    Indagine proj;

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton button_bestXY;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    // End of variables declaration//GEN-END:variables

    // End of variables declaration
    /**
     * Creates new form Jsezdlg
     * @param sv
     */
    public Jsezdlg(JSezioneView sv) {

        // super(parent, modal);
        // setUndecorated(true);
        sV = sv;
        initComponents();

        if (sV.isWhite) {
            jButton5.setText("Black background");
        } else {
            jButton5.setText("White background");
        }

        if (sV.proporz) {
            jButton7.setText("Maximize profile");
        } else {
            jButton7.setText("Proportional profile");
        }
    }

    public Jsezdlg(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        button_bestXY = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setAlwaysOnTop(true);
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                formMouseEntered(evt);
            }
        });

        jButton1.setText(org.openide.util.NbBundle.getMessage(Jsezdlg.class, "Jsezdlg.jButton1.text")); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        button_bestXY.setText(org.openide.util.NbBundle.getMessage(Jsezdlg.class, "Jsezdlg.button_bestXY.text")); // NOI18N
        button_bestXY.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                button_bestXYActionPerformed(evt);
            }
        });

        jButton3.setText(org.openide.util.NbBundle.getMessage(Jsezdlg.class, "Jsezdlg.jButton3.text")); // NOI18N
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setText(org.openide.util.NbBundle.getMessage(Jsezdlg.class, "Jsezdlg.jButton4.text")); // NOI18N
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jButton5.setText(org.openide.util.NbBundle.getMessage(Jsezdlg.class, "Jsezdlg.jButton5.text")); // NOI18N
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        jButton6.setText(org.openide.util.NbBundle.getMessage(Jsezdlg.class, "Jsezdlg.jButton6.text")); // NOI18N
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        jButton7.setText(org.openide.util.NbBundle.getMessage(Jsezdlg.class, "Jsezdlg.jButton7.text")); // NOI18N
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        jButton8.setText(org.openide.util.NbBundle.getMessage(Jsezdlg.class, "Jsezdlg.jButton8.text")); // NOI18N
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jButton7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jButton3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jButton5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton6))
                            .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButton4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButton8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(button_bestXY, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(button_bestXY)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton7)
                    .addComponent(jButton3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton6)
                    .addComponent(jButton5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton8))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

//      TODO add your handling code here:
        JVelocitaDlg velDlg = new JVelocitaDlg(proj);

        this.setVisible(false);
        velDlg.setModal(true);
        velDlg.setVisible(true);

        // velDlg.repaint();
    }//GEN-LAST:event_jButton1ActionPerformed

    public void setProj(Indagine proj) {
        this.proj = proj;
    }

    private void button_bestXYActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_button_bestXYActionPerformed

//      TODO add your handling code here:
        System.out.print(evt);
        this.setVisible(false);

        tVdlg tV = new tVdlg(tV1, tV2, spaz, proj);

        tV.setSize(550, 350);
        tV.setModal(true);
        tV.setVisible(true);

        if (tV.success == 1) {
            FileOutputStream fos = null;

            this.setVisible(false);

            String userHome = "user.home";

            // We get the path by getting the system property with the
            // defined key above. path+"/smartRefract-data/"
            String path = System.getProperty(userHome);

            try {
                fos = new FileOutputStream(path + "/smartRefract-data/" + "dromo.txt",false);

                OutputStreamWriter os = new OutputStreamWriter(fos);

                os.flush();
                os.close();
            } catch (IOException ex) {
                // Logger.getLogger(JRefractionView.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    fos.close();
                } catch (IOException ex) {
                    // Logger.getLogger(JRefractionView.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            proj.setdromo();
            proj.sezDT(false);
            sV.repaint();

            // sV.invalidate();
        }
    }//GEN-LAST:event_button_bestXYActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        FileOutputStream fos = null;

        this.setVisible(false);

        String userHome = "user.home";

        // We get the path by getting the system property with the
        // defined key above.
        String path = System.getProperty(userHome);

        try {
            fos = new FileOutputStream(path + "/smartRefract-data/" + "dromo.txt",false);

            OutputStreamWriter os = new OutputStreamWriter(fos);

            os.flush();
            os.close();
        } catch (IOException ex) {
            // Logger.getLogger(JRefractionView.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                // Logger.getLogger(JRefractionView.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        proj.setdromo();
        proj.sezDT(false);
        sV.repaint();

        // sV.invalidate();
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton4ActionPerformed

    private void formMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseEntered

        // TODO add your handling code here:
        try {
            //AWTUtilities.setWindowOpacity(this, 1f);
        } catch (Exception ex) {
        }
    }//GEN-LAST:event_formMouseEntered

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed

        // TODO add your handling code here:
        tavo_dlg = new jTavolozzaDlg(this.sV);
        this.setVisible(false);
        tavo_dlg.setModal(true);
        tavo_dlg.setVisible(true);

        if (tavo_dlg.tavDr.im[0] != null) {
            sV.bIm1 = tavo_dlg.tavDr.im[0];
            sV.dAPI.bIm1 = tavo_dlg.tavDr.im[0];
        }

        if (tavo_dlg.tavDr.im[1] != null) {
            sV.bIm2 = tavo_dlg.tavDr.im[1];
            sV.dAPI.bIm2 = tavo_dlg.tavDr.im[1];
        }

        if (tavo_dlg.tavDr.im[2] != null) {
            sV.bIm3 = tavo_dlg.tavDr.im[2];
            sV.dAPI.bIm3 = tavo_dlg.tavDr.im[2];
        }

        sV.repaint();

        // sV.invalidate();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed

        // TODO add your handling code here:
        sV.isWhite = !sV.isWhite;

        if (sV.isWhite) {
            sV.setBackground(Color.WHITE);
        } else {
            sV.setBackground(Color.BLACK);
        }

        this.setVisible(false);
        sV.obj.is_white = sV.isWhite;
        sV.repaint();

        TopComponent tc; /*= WindowManager.getDefault().findTopComponent("MyViewerTopComponent");
        Lookup tcLookup = tc.getLookup();

        ((MyViewerTopComponent) tc).gmview.is_white = sV.isWhite;
        ((MyViewerTopComponent) tc).gmview.invalidate();
        ((MyViewerTopComponent) tc).gmview.repaint();*/

        // sV.invalidate();
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed

        
        JFileChooser fc = new JFileChooser();

        /*
         * MyEditor editor = new MyEditor();
         * editor.open();
         * editor.requestActive();
         */
        fc.setFileFilter(new PNGFileFilter());
        fc.setMultiSelectionEnabled(true);
        this.setVisible(false);

        // proj.checkLic();
        if (proj.licenza) {
            int returnVal = fc.showSaveDialog(null);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                BufferedImage im_s = new BufferedImage(1300, 700, BufferedImage.TYPE_INT_RGB);    // (BufferedImage) this.createImage(this.getWidth(), this.getHeight());
                if (sV.dAPI.equals(null)) {
                    sV.dAPI = new DrawingAPI(proj, sV.obj);
                }

                // sezione.draw();
                // dAPI.drawSezAssi(g,this.getWidth(), this.getHeight());
                double[] mxmn = sV.dAPI.maxmin();
                FirstBrakeList ArrFB =  proj.stesa.get(0);

                // double[] mxmn=dAPI.maxmin();
                // FirstBrakeList ArrFB = (FirstBrakeList) proj.stesa.get(0);
                double ascismax = (ArrFB.ch - 1) * ArrFB.spaz;
                double x_step = (im_s.getWidth() / ArrFB.fb.length);
                double x = (im_s.getWidth() - (2 * sV.dAPI.margine_dx) - (2 * sV.dAPI.margine_sx)) / ascismax;
                double y = x;
                double profmx = mxmn[0] + mxmn[1];
                double rap = (profmx * y) + 120;
                double scala = 1;

                // margup=0.07
                // ystp = (ymax - (2.3 * margUp * ymax) - 30) / mxmn;

                if (rap > im_s.getHeight()) {

                    // scala=(this.getHeight()-120)/rap;
                    x = (im_s.getHeight() - 120) / profmx;
                    rap = (im_s.getHeight());
                    y = x;
                }

                if (!sV.obj.proporz) {
                    x = (im_s.getWidth() - (2 * sV.dAPI.margine_dx) - (2 * sV.dAPI.margine_sx)) / ascismax;
                    y = (im_s.getHeight() - 120) / profmx;
                    rap = (profmx * y) + 120;
                }

                im_s = new BufferedImage((int) (x * ascismax) + (2 * sV.dAPI.margine_dx) + (2 * sV.dAPI.margine_sx),
                        (int) rap, BufferedImage.TYPE_INT_RGB);

                Graphics offg = im_s.getGraphics();
                Graphics2D g2 = (Graphics2D) offg.create();

                if (sV.isWhite) {
                    g2.setBackground(Color.white);
                    g2.clearRect(0, 0, 2000, 1450);
                } else {
                    g2.setBackground(Color.BLACK);
                    g2.clearRect(0, 0, 2000, 1450);
                }



                sV.dAPI.drawSezAssi(offg, im_s.getWidth(), (int) (rap), x, y);
                sV.dAPI.drawSezione2(offg, (im_s.getWidth()), (int) (rap), x, y);

                // sV.drawAssi(offg, 2000, 1450);
                // sV.drawSezione(offg, 2000, 1450);
                try {

                    // retrieve image
                    File outputfile = fc.getSelectedFile();

                    if (!outputfile.getPath().toLowerCase().endsWith(".png")) {
                        outputfile = new File(outputfile.getPath() + ".png");
                    }

                    ImageIO.write(im_s, "png", outputfile);
                } catch (IOException e) {
                }
            }
        }

        sV.repaint();
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed

        // TODO add your handling code here:
        sV.proporz = !sV.proporz;
        sV.obj.proporz = sV.proporz;
        this.setVisible(false);
        sV.invalidate();
        sV.repaint();
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        // TODO add your handling code here:
        FineTuning ft=new FineTuning(sV.obj.proj);
        ft.setProj(proj);
        this.setVisible(false);
        ft.setParentDialog(sV);
        ft.setModal(true);
        ft.setVisible(true);
        
      
    }//GEN-LAST:event_jButton8ActionPerformed

    void color_setText(String s) {
        jButton5.setText(s);
    }

    void prop_setText(String s) {
        jButton7.setText(s);
    }

    void Apply_Tav() {
        if (tavo_dlg.tavDr.im[0] != null) {
            sV.bIm1 = tavo_dlg.tavDr.im[0];
            sV.dAPI.bIm1 = tavo_dlg.tavDr.im[0];
        }

        if (tavo_dlg.tavDr.im[1] != null) {
            sV.bIm2 = tavo_dlg.tavDr.im[1];
            sV.dAPI.bIm2 = tavo_dlg.tavDr.im[1];
        }

        if (tavo_dlg.tavDr.im[2] != null) {
            sV.bIm3 = tavo_dlg.tavDr.im[2];
            sV.dAPI.bIm3 = tavo_dlg.tavDr.im[2];
        }

        sV.repaint();

        // sV.invalidate();
    }

    /**
     * @param args the command line arguments
     */
    class PNGFileFilter extends javax.swing.filechooser.FileFilter {

        @Override
        public boolean accept(File f) {
            return f.isDirectory() || f.getName().toLowerCase().endsWith(".png");
        }

        @Override
        public String getDescription() {
            return "PNG image file";
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
